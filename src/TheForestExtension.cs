using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using uMod.Common;
using uMod.Extensions;
using UnityEngine;

namespace uMod.Game.TheForest
{
    /// <summary>
    /// The extension class that represents this extension
    /// </summary>
    [GameExtension]
    public class TheForestExtension : Extension
    {
        // Get assembly info
        internal static Assembly Assembly = Assembly.GetExecutingAssembly();
        internal static AssemblyName AssemblyName = Assembly.GetName();
        internal static VersionNumber AssemblyVersion = new VersionNumber(AssemblyName.Version.Major, AssemblyName.Version.Minor, AssemblyName.Version.Build);
        internal static string AssemblyAuthors = ((AssemblyCompanyAttribute)Attribute.GetCustomAttribute(Assembly, typeof(AssemblyCompanyAttribute), false)).Company;

        /// <summary>
        /// Gets whether this extension is for a specific game
        /// </summary>
        public override bool IsGameExtension => true;

        /// <summary>
        /// Gets the title of this extension
        /// </summary>
        public override string Title => "The Forest";

        /// <summary>
        /// Gets the author of this extension
        /// </summary>
        public override string Author => AssemblyAuthors;

        /// <summary>
        /// Gets the version of this extension
        /// </summary>
        public override VersionNumber Version => AssemblyVersion;

        /// <summary>
        /// Gets the branch of this extension
        /// </summary>
        public override string Branch => "public"; // TODO: Handle this programmatically

        /// <summary>
        /// Commands that plugins are prevented from overriding
        /// </summary>
        internal static IEnumerable<string> RestrictedCommands => new[]
        {
            ""
        };

        /// <summary>
        /// Default game-specific references for use in plugins
        /// </summary>
        public override string[] DefaultReferences => new[]
        {
            ""
        };

        /// <summary>
        /// List of namespaces allowed for use in plugins
        /// </summary>
        public override string[] WhitelistedNamespaces => new[]
        {
            "Bolt",
            "Steamworks",
            "TheForest"
        };

        /// <summary>
        /// Initializes a new instance of the TheForestExtension class
        /// </summary>
        public TheForestExtension()
        {
        }

        /// <summary>
        /// Called when this extension has been added to the specified manager
        /// </summary>
        /// <param name="manager"></param>
        public override void HandleAddedToManager(IExtensionManager manager)
        {
            manager.RegisterPluginLoader(new TheForestPluginLoader(Interface.uMod.RootLogger));
        }

        private const string logFileName = "logs/output_log.txt"; // TODO: Add -logfile support
        private TextWriter logWriter;

        /// <summary>
        /// Called when all other extensions have been loaded
        /// </summary>
        public override void OnModLoad()
        {
            // Setup logging for the game
            if (!Directory.Exists("logs"))
            {
                Directory.CreateDirectory("logs");
            }

            if (File.Exists(logFileName))
            {
                File.Delete(logFileName);
            }

            StreamWriter logStream = File.AppendText(logFileName);
            logStream.AutoFlush = true;
            logWriter = TextWriter.Synchronized(logStream);
            UnityEngine.Application.logMessageReceived += HandleLog;
        }

        public override void OnShutdown() => logWriter?.Close();

        private void HandleLog(string message, string stackTrace, LogType type)
        {
            if (!string.IsNullOrEmpty(message))
            {
                logWriter.WriteLine(message); // TODO: Fix access violation
                if (!string.IsNullOrEmpty(stackTrace))
                {
                    logWriter.WriteLine(stackTrace);
                }
            }
        }
    }
}

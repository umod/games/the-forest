﻿using Steamworks;
using System;
using System.Globalization;
using System.Linq;
using TheForest.Utils;
using uMod.Auth;
using uMod.Common;
using uMod.Text;
using uMod.Unity;
using UnityEngine;

namespace uMod.Game.TheForest
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class TheForestPlayer : UniversalPlayer, IPlayer
    {
        #region Initialization

        private readonly CSteamID cSteamId;
        private readonly PlayerStats stats;
        private readonly ulong steamId;

        internal BoltEntity boltEntity;

        public TheForestPlayer(string playerId, string playerName)
        {
            // Store player details
            Id = playerId;
            Name = playerName?.Sanitize() ?? "Unnamed"; // TODO: Localization
            ulong.TryParse(playerId, out steamId);
        }

        public TheForestPlayer(BoltEntity boltEntity)
        {
            // Store player object
            this.boltEntity = boltEntity;
            cSteamId = SteamDSConfig.clientConnectionInfo[boltEntity.source.ConnectionId];
            steamId = cSteamId.m_SteamID;
            Id = steamId.ToString();
            Name = (boltEntity.GetState<IPlayerState>().name?.Sanitize() ?? Name) ?? "Unnamed"; // TODO: Localization
            stats = boltEntity.GetComponentInChildren<PlayerStats>();
        }

        #endregion Initialization

        #region Objects

        /// <summary>
        /// Gets the object that backs the player
        /// </summary>
        public object Object { get; set; }

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get; set; }

        /// <summary>
        /// Reconnects the gamePlayer to the player object
        /// </summary>
        /// <param name="gamePlayer"></param>
        public void Reconnect(object gamePlayer)
        {
            // Reconnect player objects
            Object = gamePlayer;
            boltEntity = gamePlayer as BoltEntity;
        }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public override string Id { get; }

        /// <summary>
        /// Gets the language for the player
        /// </summary>
        public CultureInfo Language => CultureInfo.GetCultureInfo("en"); // TODO: Implement when possible

        /// <summary>
        /// Gets the IP address for the player
        /// </summary>
        public string Address
        {
            get
            {
                SteamGameServerNetworking.GetP2PSessionState(cSteamId, out P2PSessionState_t sessionState);
                uint ip = sessionState.m_nRemoteIP;
                return string.Concat(ip >> 24 & 255, ".", ip >> 16 & 255, ".", ip >> 8 & 255, ".", ip & 255);
            }
        }

        /// <summary>
        /// Gets the average network ping for the player
        /// </summary>
        public int Ping => Convert.ToInt32(boltEntity.source.PingNetwork);

        /// <summary>
        /// Gets if the player is a server admin
        /// </summary>
        public override bool IsAdmin => boltEntity != null && boltEntity.source.IsDedicatedServerAdmin()
            || BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Administrators);

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        public override bool IsModerator => IsAdmin || BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Moderators);

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => CoopKick.IsBanned(steamId);

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        public bool IsConnected => boltEntity != null ? BoltNetwork.clients.Contains(boltEntity.source) : false;

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        public bool IsSleeping => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        public bool IsServer => false;

        /// <summary>
        /// Gets/sets if the player has connected before
        /// </summary>
        public bool IsReturningPlayer { get; set; }

        /// <summary>
        /// Gets/sets if the player has spawned before
        /// </summary>
        public bool HasSpawnedBefore { get; set; }

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason = "", TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned)
            {
                // Ban player
                CoopKick.KickedPlayer kickedPlayer = new CoopKick.KickedPlayer
                {
                    Name = Name,
                    SteamId = steamId,
                    BanEndTime = duration.TotalMinutes <= 0 ? 0 : DateTime.UtcNow.ToUnixTimestamp() + (long)duration.TotalMinutes
                };
                CoopKick.Instance.kickedSteamIds.Add(kickedPlayer);
                CoopKick.SaveList();

                // Kick player with reason
                Kick(reason);
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining
        {
            get
            {
                CoopKick.KickedPlayer kickedPlayer = CoopKick.Instance.KickedPlayers.First(x => x.SteamId == steamId);
                return kickedPlayer != null ? TimeSpan.FromTicks(kickedPlayer.BanEndTime) : TimeSpan.Zero;
            }
        }

        /// <summary>
        /// Kicks the player from the game
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason = "")
        {
            if (IsConnected)
            {
                CoopKickToken coopKickToken = new CoopKickToken
                {
                    KickMessage = reason,
                    Banned = false
                };
                boltEntity?.source.Disconnect(coopKickToken);
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if already unbanned
            if (IsBanned)
            {
                // Unban player
                CoopKick.KickedPlayer kickedPlayer = CoopKick.Instance.kickedSteamIds.First(x => x.SteamId == steamId);
                if (kickedPlayer != null)
                {
                    CoopKick.Instance.kickedSteamIds.Remove(kickedPlayer);
                    CoopKick.SaveList();
                }
            }
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            /*GameObject deadTriggerObject = player.DeadTriggerObject;
            if (deadTriggerObject != null && deadTriggerObject.activeSelf)
            {
                RespawnDeadTrigger component = deadTriggerObject.GetComponent<RespawnDeadTrigger>();
                PlayerHealed phealed = PlayerHealed.Create(GlobalTargets.Others);
                phealed.HealingItemId = component._healItemId;
                phealed.HealTarget = player.Entity;
                phealed.Send();
                component.SendMessage("SetActive", false);
            }*/

            get => stats != null ? stats.Health : 0f;
            set
            {
                if (stats != null)
                {
                    stats.Health = value; // TODO: Test
                }
            }
        }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get
            {
                return 1000f; // TODO: Implement when possible
            }
            set
            {
                throw new NotImplementedException(); // TODO: Implement when possible
            }
        }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount)
        {
            if (stats != null)
            {
                stats.Health += amount; // TODO: Test
            }
        }

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount) => stats?.Hit((int)amount, true); // TODO: Test

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill() => Hurt(1000f);

        /// <summary>
        /// Renames the player to specified name
        /// <param name="newName"></param>
        /// </summary>
        public void Rename(string newName)
        {
            if (boltEntity != null)
            {
                boltEntity.GetState<IPlayerState>().name = newName; // TODO: Test
            }
        }

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset()
        {
            // TODO: Implement when possible
        }

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn()
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position()
        {
            return (boltEntity?.gameObject?.transform?.position ?? Vector3.zero).ToPosition();
        }

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            Position pos = Position();
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(float x, float y, float z)
        {
            if (IsConnected && boltEntity?.transform != null)
            {
                //entity.transform.localPosition = new Vector3(x, y, z); // TODO: Fix if the game ever supports this
                throw new NotImplementedException(); // TODO: Implement when possible
            }
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                // Create and send the message
                message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), args) : Formatter.ToUnity(message);
                ChatEvent chatEvent = ChatEvent.Create(boltEntity.source);
                chatEvent.Message = prefix != null ? $"{prefix}: {message}" : message;
                chatEvent.Sender = boltEntity.networkId;
                chatEvent.Send();
            }
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args) => Message(message, prefix, args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Message(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command) && IsConnected)
            {
                AdminCommand adminCommand = AdminCommand.Create(boltEntity.source);
                adminCommand.Command = command;
                adminCommand.Data = string.Join(" ", Array.ConvertAll(args, x => x.ToString()));
                adminCommand.Send();
            }
        }

        #endregion Chat and Commands
    }
}

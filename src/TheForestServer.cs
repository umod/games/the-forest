﻿using Bolt;
using System;
using System.Globalization;
using System.Linq;
using System.Net;
using TheForest.Utils;
using UdpKit;
using uMod.Common;
using uMod.Text;
using UnityEngine;
using WebResponse = uMod.Common.Web.WebResponse;

namespace uMod.Game.TheForest
{
    /// <summary>
    /// Represents the server hosting the game instance
    /// </summary>
    public class TheForestServer : IServer
    {
        /// <summary>
        /// Gets or sets player manager
        /// </summary>
        public IPlayerManager PlayerManager { get; set; }

        #region Server Information

        /// <summary>
        /// Gets/sets the public-facing name of the server
        /// </summary>
        public string Name
        {
            get => CoopLobby.Instance.Info.Name ?? SteamDSConfig.ServerName;
            set => CoopLobby.Instance.SetName(value);
        }

        private static IPAddress address;
        private static IPAddress localAddress;

        /// <summary>
        /// Gets the public-facing IP address of the server, if known
        /// </summary>
        public IPAddress Address
        {
            get
            {
                try
                {
                    if (address == null || !Utility.ValidateIPv4(address.ToString()))
                    {
                        if (Utility.ValidateIPv4(SteamDSConfig.ServerAddress) && !Utility.IsLocalIP(SteamDSConfig.ServerAddress))
                        {
                            IPAddress.TryParse(SteamDSConfig.ServerAddress, out address);
                            Interface.uMod.LogInfo($"IP address from server: {address}"); // TODO: Localization
                        }
                        else
                        {
                            Web.Client webClient = new Web.Client();
                            webClient.Get("https://api.ipify.org")
                               .Done(delegate (WebResponse response)
                               {
                                   if (response.StatusCode == 200)
                                   {
                                       IPAddress.TryParse(response.ReadAsString(), out address);
                                       Interface.uMod.LogInfo($"IP address from external API: {address}"); // TODO: Localization
                                   }
                               });
                        }
                    }

                    return address;
                }
                catch (Exception ex)
                {
                    Interface.uMod.LogWarning("Couldn't get server's public IP address", ex); // TODO: Localization
                    return IPAddress.Any;
                }
            }
        }

        /// <summary>
        /// Gets the local IP address of the server, if known
        /// </summary>
        public IPAddress LocalAddress
        {
            get
            {
                try
                {
                    return localAddress ?? (localAddress = Utility.GetLocalIP());
                }
                catch
                {
                    return IPAddress.Any;
                }
            }
        }

        /// <summary>
        /// Gets the public-facing network port of the server, if known
        /// </summary>
        public ushort Port => SteamDSConfig.ServerGamePort;

        /// <summary>
        /// Gets the version or build number of the server
        /// </summary>
        public string Version => SteamDSConfig.ServerVersion;

        /// <summary>
        /// Gets the network protocol version of the server
        /// </summary>
        public string Protocol => Version;

        /// <summary>
        /// Gets the language set by the server
        /// </summary>
        public CultureInfo Language => CultureInfo.GetCultures(CultureTypes.AllCultures).FirstOrDefault(x => x.EnglishName == global::Localization.language);

        /// <summary>
        /// Gets the total of players currently on the server
        /// </summary>
        public int Players => CoopLobby.Instance.Info.CurrentMembers;

        /// <summary>
        /// Gets/sets the maximum players allowed on the server
        /// </summary>
        public int MaxPlayers
        {
            get => CoopLobby.Instance.Info.MemberLimit;
            set => CoopLobby.Instance.SetMemberLimit(value);
        }

        /// <summary>
        /// Gets/sets the current in-game time on the server
        /// </summary>
        public DateTime Time
        {
            get => DateTime.Today.AddMinutes(Scene.Atmosphere.TimeOfDay); // TODO: Fix this not working
            set => Scene.Atmosphere.TimeOfDay = value.Minute; // TODO: Fix this not working
        }

        /// <summary>
        /// Gets the current or average frame rate of the server
        /// </summary>
        public int FrameRate
        {
            get => Mathf.RoundToInt(1f / UnityEngine.Time.smoothDeltaTime);
        }

        /// <summary>
        /// Gets/sets the target frame rate for the server
        /// </summary>
        public int TargetFrameRate
        {
            get => UnityEngine.Application.targetFrameRate;
            set => UnityEngine.Application.targetFrameRate = value;
        }

        /// <summary>
        /// Gets information on the currently loaded save file
        /// </summary>
        ISaveInfo IServer.SaveInfo => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Shuts down the server, with optional saving and delay
        /// </summary>
        public void Shutdown(bool save = true, int delay = 0)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        #endregion Server Information

        #region Server Administration

        /// <summary>
        /// Saves the server and any related information
        /// </summary>
        public void Save()
        {
            LevelSerializer.Checkpoint();
            SteamDSConfig.SaveGame();
        }

        #endregion Server Administration

        #region Player Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string playerId, string reason, TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned(playerId))
            {
                if (ulong.TryParse(playerId, out ulong steamId))
                {
                    // Ban player
                    Scene.HudGui.MpPlayerList.Ban(steamId);
                    CoopKick.SaveList();
                }
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        /// <param name="playerId"></param>
        public TimeSpan BanTimeRemaining(string playerId)
        {
            if (ulong.TryParse(playerId, out ulong steamId))
            {
                CoopKick.KickedPlayer kickedPlayer = CoopKick.Instance.KickedPlayers.First(p => p.SteamId == steamId);
                return kickedPlayer != null ? TimeSpan.FromTicks(kickedPlayer.BanEndTime) : TimeSpan.Zero;
            }

            return TimeSpan.Zero;
        }

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        /// <param name="playerId"></param>
        public bool IsBanned(string playerId)
        {
            return ulong.TryParse(playerId, out ulong steamId) && CoopKick.IsBanned(new UdpSteamID(steamId));
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        /// <param name="playerId"></param>
        public void Unban(string playerId)
        {
            // Check if already unbanned
            if (IsBanned(playerId))
            {
                if (ulong.TryParse(playerId, out ulong steamId))
                {
                    // Unban player
                    CoopKick.UnBanPlayer(steamId);
                }
            }
        }

        #endregion Player Administration

        #region Chat and Commands

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, string prefix, ulong id, params object[] args)
        {
            if (!string.IsNullOrEmpty(message))
            {
                // Format the message
                message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), args) : Formatter.ToUnity(message);

                foreach (BoltEntity entity in BoltNetwork.entities)
                {
                    if (entity != null && entity.StateIs<IPlayerState>())
                    {
                        // Create and send the message
                        ChatEvent chatEvent = ChatEvent.Create(entity.source);
                        chatEvent.Message = prefix != null ? $"{prefix}: {message}" : message;
                        chatEvent.Sender = entity.networkId;
                        chatEvent.Send();
                    }
                }

                Interface.uMod.LogInfo($"[Broadcast] {message}");
            }
        }

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, string prefix, params object[] args) => Broadcast(message, prefix, 0uL, args);

        /// <summary>
        /// Broadcasts the specified chat message to all players with the specified icon
        /// </summary>
        /// <param name="message"></param>
        /// <param name="id"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, ulong id, params object[] args) => Broadcast(message, string.Empty, id, args);

        /// <summary>
        /// Broadcasts the specified chat message to all players
        /// </summary>
        /// <param name="message"></param>
        public void Broadcast(string message) => Broadcast(message, null);

        /// <summary>
        /// Runs the specified server command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command))
            {
                AdminCommand adminCommand = AdminCommand.Create(GlobalTargets.OnlyServer);
                adminCommand.Command = command;
                adminCommand.Data = string.Join(" ", Array.ConvertAll(args, x => x.ToString()));
                adminCommand.Send();
            }
        }

        #endregion Chat and Commands
    }
}

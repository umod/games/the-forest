﻿using uMod.Auth;
using uMod.Common;

namespace uMod.Game.TheForest
{
    /// <summary>
    /// Represents a TheForest player manager
    /// </summary>
    public class TheForestPlayerManager : PlayerManager<TheForestPlayer>
    {
        /// <summary>
        /// Create a new instance of the TheForestPlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public TheForestPlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }
    }
}

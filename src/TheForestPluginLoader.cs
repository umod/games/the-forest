﻿using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Game.TheForest
{
    /// <summary>
    /// Responsible for loading the core plugin
    /// </summary>
    public class TheForestPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(TheForest) };

        public TheForestPluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}

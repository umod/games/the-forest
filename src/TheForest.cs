﻿using Steamworks;
using System;
using System.Linq;
using TheForest.Utils;
using uMod.Common;
using uMod.Configuration;
using uMod.Plugins;
using uMod.Plugins.Decorators;

namespace uMod.Game.TheForest
{
    /// <summary>
    /// The core The Forest plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class TheForest : Plugin
    {
        #region Initialization

        internal static readonly TheForestProvider Universal = TheForestProvider.Instance;

        private bool serverInitialized;

        /// <summary>
        /// Initializes a new instance of the TheForestCore class
        /// </summary>
        public TheForest()
        {
            // Set plugin info attributes
            Title = "The Forest";
            Author = TheForestExtension.AssemblyAuthors;
            Version = TheForestExtension.AssemblyVersion;
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this);

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();

            serverInitialized = true;
        }

        #endregion Initialization

        #region Player Hooks

        /// <summary>
        /// Called when the player is attempting to connect
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        [Hook("IOnUserApprove")]
        private object IOnUserApprove(BoltConnection connection)
        {
            CSteamID cSteamId = SteamDSConfig.clientConnectionInfo[connection.ConnectionId];
            string playerId = cSteamId.ToString();

            // Check for existing player's name
            IPlayer player = Players.FindPlayerById(playerId);
            string playerName = !string.IsNullOrEmpty(player?.Name) ? player.Name : "Unnamed"; // TODO: Localization

            Players.PlayerJoin(playerId, playerName);

            // Get IP address from Steam
            SteamGameServerNetworking.GetP2PSessionState(cSteamId, out P2PSessionState_t sessionState);
            uint remoteIp = sessionState.m_nRemoteIP;
            string ipAddress = string.Concat(remoteIp >> 24 & 255, ".", remoteIp >> 16 & 255, ".", remoteIp >> 8 & 255, ".", remoteIp & 255);

            // Call pre hook for plugins
            object loginUniversal = Interface.CallHook("CanClientLogin", playerName, playerId, ipAddress);
            object loginDeprecated = Interface.CallDeprecatedHook("CanUserLogin", "CanClientLogin", new DateTime(2021, 1, 1), playerName, playerId, ipAddress);
            object canLogin = loginUniversal is null ? loginDeprecated : loginUniversal;

            // Can the player log in?
            if (!serverInitialized || canLogin is string || canLogin is bool loginBlocked && !loginBlocked)
            {
                // Create kick token for player
                CoopKickToken coopKickToken = new CoopKickToken
                {
                    KickMessage = !serverInitialized ? "Server not initialized yet" : canLogin is string ? canLogin.ToString() : "Connection was rejected", // TODO: Localization
                    Banned = false
                };

                // Disconnect player using kick token
                connection.Disconnect(coopKickToken);
                return true;
            }

            // Call post hook for plugins
            Interface.CallHook("OnPlayerApproved", playerName, playerId, ipAddress);
            Interface.CallDeprecatedHook("OnUserApprove", "OnPlayerApproved", new DateTime(2020, 6, 1), connection);
            Interface.CallDeprecatedHook("OnUserApproved", "OnPlayerApproved", new DateTime(2021, 1, 1), playerName, playerId, ipAddress);

            return null;
        }

        /// <summary>
        /// Called when the player has connected
        /// </summary>
        /// <param name="connection"></param>
        [Hook("IOnPlayerConnected")]
        private void IOnPlayerConnected(BoltConnection connection)
        {
            string playerId = SteamDSConfig.clientConnectionInfo[connection.ConnectionId].m_SteamID.ToString();

            // Set default groups, if necessary
            GroupProvider defaultGroups = Interface.uMod.Auth.Configuration.Groups;
            if (!permission.UserHasGroup(playerId, defaultGroups.Players))
            {
                permission.AddUserGroup(playerId, defaultGroups.Players);
            }
            if (connection.IsDedicatedServerAdmin() && !permission.UserHasGroup(playerId, defaultGroups.Administrators))
            {
                permission.AddUserGroup(playerId, defaultGroups.Administrators);
            }

            IPlayer player = Players.FindPlayerById(playerId);
            if (player != null)
            {
                // Set IPlayer object in Player
                //connection. = player; // TODO: Get BoltEntity to set IPlayer

                Players.PlayerConnected(player.Id, player);

                // Call hook for plugins
                Interface.CallHook("OnPlayerConnected", player);
                Interface.CallDeprecatedHook("OnUserConnected", "OnPlayerConnected", new DateTime(2021, 1, 1), player);
            }
        }

        /// <summary>
        /// Called when the player has respawned
        /// </summary>
        /// <param name="entity"></param>
        [Hook("IOnPlayerRespawn")]
        private void IOnPlayerRespawn(BoltEntity entity)
        {
            string playerId = SteamDSConfig.clientConnectionInfo[entity.source.ConnectionId].m_SteamID.ToString();

            IPlayer player = Players.FindPlayerById(playerId);
            if (player != null)
            {
                // Set IPlayer object in BoltEntity
                entity.IPlayer = player;

                // Call hook for plugins
                Interface.CallHook("OnPlayerRespawn", player);
                Interface.CallDeprecatedHook("OnUserRespawn", "OnPlayerRespawn", new DateTime(2021, 1, 1), player);
            }
        }

        /// <summary>
        /// Called when the player spawns
        /// </summary>
        /// <param name="entity"></param>
        [Hook("OnPlayerSpawn")]
        private void OnPlayerSpawn(BoltEntity entity)
        {
            IPlayer player = entity.IPlayer;
            if (player != null)
            {
                // Set player name if available
                player.Name = entity.GetState<IPlayerState>().name?.Sanitize() ?? (!string.IsNullOrEmpty(player.Name) ? player.Name : "Unnamed"); // TODO: Localization

                // Call hook for plugins
                Interface.CallHook("OnPlayerSpawn", player);
                Interface.CallDeprecatedHook("OnUserSpawn", "OnPlayerSpawn", new DateTime(2021, 1, 1), player);
            }
        }

        /// <summary>
        /// Called when the player sends a message
        /// </summary>
        /// <param name="evt"></param>
        [Hook("IOnPlayerChat")]
        private object IOnPlayerChat(ChatEvent evt)
        {
            if (evt.Message.Trim().Length <= 1)
            {
                return true;
            }

            BoltEntity entity = BoltNetwork.FindEntity(evt.Sender);
            if (entity == null)
            {
                return null;
            }

            IPlayer player = entity.IPlayer;
            if (player == null)
            {
                return null;
            }

            // Set player name if available
            player.Name = entity.GetState<IPlayerState>().name?.Sanitize() ?? (!string.IsNullOrEmpty(player.Name) ? player.Name : "Unnamed"); // TODO: Localization

            // Is the chat message a command?
            if (Universal.CommandSystem.HandleChatMessage(player, evt.Message) == CommandState.Completed)
            {
                return true;
            }

            // Call hook for plugins
            object chatUniversal = Interface.CallHook("OnPlayerChat", player, evt.Message);
            object chatDeprecated = Interface.CallDeprecatedHook("OnUserChat", "OnPlayerChat", new DateTime(2021, 1, 1), player, evt.Message);
            object canChat = chatUniversal is null ? chatDeprecated : chatUniversal;

            // Is the chat message blocked?
            if (canChat != null)
            {
                return true;
            }

            Interface.uMod.LogInfo($"[Chat] {player.Name}: {evt.Message}");
            return null;
        }

        /// <summary>
        /// Called when the player has disconnected
        /// </summary>
        /// <param name="connection"></param>
        [Hook("IOnPlayerDisconnected")]
        private void IOnPlayerDisconnected(BoltConnection connection)
        {
            BoltEntity entity = Scene.SceneTracker.allPlayerEntities.FirstOrDefault(ent => ent.source.ConnectionId == connection.ConnectionId);
            if (entity == null)
            {
                return;
            }

            IPlayer player = entity.IPlayer;
            if (player == null)
            {
                return;
            }

            // Set player name if available
            player.Name = entity.GetState<IPlayerState>().name?.Sanitize() ?? (!string.IsNullOrEmpty(player.Name) ? player.Name : "Unnamed"); // TODO: Localization

            // Call hook for plugins
            Interface.CallHook("OnPlayerDisconnected", player);
            Interface.CallDeprecatedHook("OnUserDisconnected", "OnPlayerDisconnected", new DateTime(2021, 1, 1), player);

            Interface.uMod.LogInfo($"{player.Id}/{player.Name} quit");

            Players.PlayerDisconnected(player.Id);
        }

        #endregion Player Hooks

        #region Server Hooks

        /// <summary>
        /// Called when a command was run from the server
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        [Hook("IOnServerCommand")]
        private object IOnServerCommand(BoltConnection connection, string command, string args)
        {
            if (command.Length != 0)
            {
                // Is the server commanda blocked?
                if (Interface.CallHook("OnServerCommand", command, args.Split(' ')) != null)
                {
                    return true;
                }

                // Handle the command
                IPlayer player = new TheForestConsolePlayer();
                if (Universal.CommandSystem.HandleConsoleMessage(player, $"{command} {args}") == CommandState.Completed)
                {
                    return true;
                }
            }

            return null;
        }

        #endregion Server Hooks
    }
}
